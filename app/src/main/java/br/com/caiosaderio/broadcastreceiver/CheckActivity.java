package br.com.caiosaderio.broadcastreceiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;


public class CheckActivity extends AppCompatActivity {

    private static ImageView internetConnection;
    private static ImageView usbConnection;
//    private internetReceiver iReceiver;
//    private usbReceiver uReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);

        internetConnection = (ImageView) findViewById(R.id.internetConnection);
        usbConnection = (ImageView) findViewById(R.id.usbConnection);


        Intent internet = new Intent(this, internetReceiver.class);
        sendBroadcast(internet);

        Intent usb = new Intent(this, usbReceiver.class);
        sendBroadcast(usb);
    }

    public static void updateInternetImage(Boolean hasConnection){
        if(hasConnection){
            internetConnection.setImageResource(R.drawable.rede_on);
        }else{
            internetConnection.setImageResource(R.drawable.rede_off);
        }
    }

    public static void updateUsbImage(Boolean hasConnection){
        if(hasConnection){
            usbConnection.setImageResource(R.drawable.usb_on);
        }else{
            usbConnection.setImageResource(R.drawable.usb_off);
        }
    }

    public static class internetReceiver extends BroadcastReceiver {

        private ConnectivityManager cm;
        private Boolean networkStatus = false;

        public internetReceiver(){

        }


        public Boolean getNetworkStatus() {
            return networkStatus;
        }

        public void setNetworkStatus(Boolean networkStatus) {
            this.networkStatus = networkStatus;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork= cm.getActiveNetworkInfo();
            setNetworkStatus(activeNetwork != null && activeNetwork.isConnected());
            updateInternetImage(getNetworkStatus());
        }
    }

    public static class usbReceiver extends BroadcastReceiver {

        private UsbManager usb;
        private Boolean usbStatus = false;

        public usbReceiver(){

        }

        public Boolean getUsbStatus() {
            return usbStatus;
        }

        public void setUsbStatus(Boolean usbStatus) {
            this.usbStatus = usbStatus;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getExtras() != null){
                setUsbStatus(intent.getExtras().getBoolean("connected"));
            }
            //Toast.makeText(context, "Teste do USB caramba "+getUsbStatus().toString(), Toast.LENGTH_LONG).show();
            updateUsbImage(getUsbStatus());
        }
    }

}
